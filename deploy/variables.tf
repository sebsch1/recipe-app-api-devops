variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email@londonappdev.com"
}

variable "db_username" {
  description = "Username for the RDS postg"
}

variable "db_password" {
  description = "Username for the RDS postg"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "713257143926.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR iamge for proxy"
  default     = "713257143926.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}